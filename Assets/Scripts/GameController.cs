﻿using UnityEngine; using System.Collections; using System.Collections.Generic; using UnityEngine.UI;


public class GameController : MonoBehaviour {
	public static GameController Instance;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	public bool Button;

	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private Text levelText;
	private GameObject startImage;
	private GameObject button;
	private Text loadText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 1;
	private int currentLevel = 1;

	// Use this for initialization
	void Awake () {
		if (Instance != null && Instance != this) {
			DestroyImmediate (gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad(gameObject);
		boardController = GetComponent <BoardController> ();
		enemies = new List<Enemy>();
	}
	void Start(){
		StartGameImage ();
		
	}
	public void StartGameImage(){
			//button = GameObject.Find ("Button");
			startImage = GameObject.Find ("Start Image");
			startImage.SetActive (true);
		
	}
	private void DisableStartImage(){
		Invoke ("DisableStartImage", secondsUntilLevelStart);

		startImage.SetActive (false);
		InitializeGame();
	}
	private void InitializeGame(){
		if (currentLevel > 1) {
			startImage = GameObject.Find ("Start Image");
			startImage.SetActive(false);
		}
			settingUpGame = true;
			levelImage = GameObject.Find ("Level Image");
			levelText = GameObject.Find ("Level Text").GetComponent<Text> ();
			levelText.text = "Day " + currentLevel;
			levelImage.SetActive (true);
			enemies.Clear ();
			boardController.SetupLevel (currentLevel);
			Invoke ("DisableLevelImage", secondsUntilLevelStart);
		

	}

	private void DisableLevelImage(){
		levelImage.SetActive (false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}
	private void OnLevelWasLoaded(int levelLoaded){
		currentLevel++;
		InitializeGame ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isPlayerTurn || areEnemiesMoving || settingUpGame) {
			return;
		}
		StartCoroutine (MoveEnemies ());
	}
	private IEnumerator MoveEnemies(){
		areEnemiesMoving = true;

		yield return new WaitForSeconds(0.2f);
		//code to make all enemies move on the game board
		foreach (Enemy enemy in enemies){
			enemy.MoveEnemy();
			yield return new WaitForSeconds(enemy.moveTime);

		}
		areEnemiesMoving = false;
		isPlayerTurn = true;
	}
	public void AddEnemyToList(Enemy enemy){
		enemies.Add (enemy);
	}
	public void GameOver(){
		SoundController.Instance.music.Stop ();
		SoundController.Instance.PlaySingle (gameOverSound);
		levelText.text = "You starved after " + currentLevel + " days...";
		levelImage.SetActive (true);
		enabled = false;
	}
}
